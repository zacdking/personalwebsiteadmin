
## Personal Website Admin

---
### System Dependencies:

Microsoft Visual Studio Community 2015 Version 14.0.25431.01 Update 3

---
### Description:
This project consists of an app that communicates with the Personal Website Ruby on Rails database. It currently has access to the Comments and Expressions tables and can perform delete and edit operations. 

---
### Usage:
The settings menu consists of a single textbox that should be set to a Base URL that makes up a Personal Website. API commands are appended to the Base URL to produce results. If a Base URL is empty (upon initially starting up the app), the settings menu will appear before the main menu. 

The View tab in the menu strip will allow you select between different tables in the database. Selecting one will automatically populate the data grid. The buttons at the bottom of the main menu will now be usable:
 
* Double clicking or pressing the edit button while having a single record highlighted will allow you to edit that record and update it.
* Having any amount of records selected and hitting the delete button will instantly delete all selected items.
* Clear table button will delete all records in the selected table
* Wipe Database will delete all records in all tables 
