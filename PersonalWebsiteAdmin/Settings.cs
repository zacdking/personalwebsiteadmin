﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PersonalWebsiteAdmin
{
    public partial class Settings : Form
    {
        #region constructors

        public Settings()
        {
            InitializeComponent();
            GetSettings();
        }

        #endregion

        #region private methods 

        private void GetSettings()
        {
            personalWebsiteURLTextBox.Text = Properties.Settings.Default.personalWebsiteURL;
        }

        private void SaveSettings()
        {
            Properties.Settings.Default.personalWebsiteURL = personalWebsiteURLTextBox.Text;

            Properties.Settings.Default.Save();
        }

        #endregion

        #region events

        private void cancelButton_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void saveButton_Click(object sender, EventArgs e)
        {
            SaveSettings();
            Close();
        }

        #endregion
    }
}
