﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.IO;
using Newtonsoft.Json;

namespace PersonalWebsiteAdmin
{
    static class PersonalWebsiteAPI
    {
        public enum Model
        {
            Comment,
            Expression
        }
        
        #region public methods

        /// <summary>
        /// Gets all comments in Personal Website database via API
        /// </summary>
        /// <param name="personalWebsiteURL">Base URL to Personal Website</param>
        /// <returns>List of comments in Personal Website database</returns>
        public static List<Comment> GetComments(string personalWebsiteURL)
        {
            string commentsRequest = personalWebsiteURL + "/api/v1/comments/";
            string responseString = makeWebRequest(commentsRequest);

            dynamic jsonData = JsonConvert.DeserializeObject(responseString);

            return jsonToComments(jsonData);
        }

        /// <summary>
        /// Gets all expressions in Personal Website database via API
        /// </summary>
        /// <param name="personalWebsiteURL">Base URL to Personal Website</param>
        /// <returns>List of expressions in Personal Website database</returns>
        public static List<Expression> GetExpressions(string personalWebsiteURL)
        {
            string expressionsRequest = personalWebsiteURL + "/api/v1/expressions/";
            string responseString = makeWebRequest(expressionsRequest);

            dynamic jsonData = JsonConvert.DeserializeObject(responseString);

            return jsonToExpressions(jsonData);
        }

        /// <summary>
        /// Updates a comment in Personal Website database via API
        /// </summary>
        /// <param name="personalWebsiteURL">Base URL to Personal Website</param>
        /// <param name="comment">Comment with appropiate ID and new values to save over old values</param>
        /// <returns>Bool representing success of method</returns>
        public static bool UpdateComment(string personalWebsiteURL, Comment comment)
        {
            bool success = true;
            string updateRequest = personalWebsiteURL + "/api/v1/comments/" + comment.id.ToString();
            string requestBody = JsonConvert.SerializeObject(comment);
            string responseString = makeWebRequest(updateRequest, "PUT", "application/json", requestBody);

            dynamic jsonData = JsonConvert.DeserializeObject(responseString);

            if (jsonData["error"] != null)
                success = false;

            return success;
        }

        /// <summary>
        /// Deletes a record in Personal Website database via API
        /// </summary>
        /// <param name="personalWebsiteURL">Base URL to Personal Website</param>
        /// <param name="model">Model of record to be deleted</param>
        /// <param name="recordId">Id of record to be deleted</param>
        /// <returns>Bool representing success</returns>
        public static bool DeleteRecord(string personalWebsiteURL, Model model, int recordId)
        {
            bool success = true;
            string deleteRequest = personalWebsiteURL + "/api/v1/";

            if (model == Model.Comment)
                deleteRequest += "comments/" + recordId.ToString();
            else if (model == Model.Expression)
                deleteRequest += "expressions/" + recordId.ToString();

            string responseString = makeWebRequest(deleteRequest, "DELETE");

            dynamic jsonData = JsonConvert.DeserializeObject(responseString);

            // If json returned has an error key
            if (jsonData["error"] != null)
                success = false;

            return success;
        }

        /// <summary>
        /// Deletes a table in Personal Website database
        /// </summary>
        /// <param name="personalWebsiteURL">Base URL to Personal Website</param>
        /// <param name="model">Model of table to be deleted</param>
        /// <returns>Bool representing success</returns>
        public static bool DeleteAll(string personalWebsiteURL, Model model)
        {
            bool success = true;

            string deleteAllRequest = personalWebsiteURL + "/api/v1/";

            if (model == Model.Comment)
                deleteAllRequest += "comments/destroy_all";
            else if (model == Model.Expression)
                deleteAllRequest += "expressions/destroy_all";

            string responseString = makeWebRequest(deleteAllRequest, "DELETE");

            dynamic jsonData = JsonConvert.DeserializeObject(responseString);

            // If json returned has an error key
            if (jsonData["error"] != null)
                success = false;

            return success;
        }

        #endregion

        #region private methods

        /// <summary>
        /// 
        /// </summary>
        /// <param name="url">Full URL to Personal Website</param>
        /// <param name="HttpMethod">GET, POST, PUT, DELETE, etc</param>
        /// <param name="contentType">Indicates media type of the resource</param>
        /// <param name="body">Information to be sent with request</param>
        /// <returns>String of request's response</returns>
        private static string makeWebRequest(string url, string HttpMethod = "GET", string contentType = null, string body = null)
        {
            var request = WebRequest.Create(url);

            request.Method = HttpMethod;
            request.ContentType = contentType;

            // If body exists
            if (!string.IsNullOrEmpty(body))
            {
                // Converts body string to bytes
                ASCIIEncoding encoding = new ASCIIEncoding();
                byte[] byte1 = encoding.GetBytes(body);

                // Set the content length of the string being posted.
                request.ContentLength = byte1.Length;

                Stream newStream = request.GetRequestStream();
                newStream.Write(byte1, 0, byte1.Length);
            }

            // Send request, get response 
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            Stream dataStream = response.GetResponseStream();
            StreamReader reader = new StreamReader(dataStream);
            string responseString = reader.ReadToEnd();

            dataStream.Close();
            reader.Close();

            return responseString;
        }

        /// <summary>
        /// Converts json data to a List of comments
        /// </summary>
        /// <param name="jsonData">json data that include comments</param>
        /// <returns>List of comments</returns>
        private static List<Comment> jsonToComments(dynamic jsonData)
        {
            List<Comment> comments = new List<Comment>();

            foreach(var comment in jsonData.comments)
            {
                comments.Add(new Comment
                {
                    id = comment.id,
                    subject = comment.subject,
                    author = comment.author,
                    body = comment.body
                });
            }

            return comments;
        }

        /// <summary>
        /// Converts json data to a List of comments
        /// </summary>
        /// <param name="jsonData">json data that include expressions</param>
        /// <returns>List of expressions</returns>
        private static List<Expression> jsonToExpressions(dynamic jsonData)
        {
            List<Expression> expressions = new List<Expression>();

            foreach(var expression in jsonData.expressions)
            {
                expressions.Add(new PersonalWebsiteAdmin.Expression {
                    id = expression.id,
                    full = expression.full,
                    simplified = expression.simplified,
                    answer = expression.answer
                });
            }

            return expressions;
        }

        #endregion
    }
}
