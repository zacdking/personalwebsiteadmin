﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersonalWebsiteAdmin
{
    public class Expression
    {
        public int id { get; set; }
        public string full { get; set; }
        public string simplified { get; set; }
        public string answer { get; set; }
    }
}
