﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersonalWebsiteAdmin
{
    public class Comment
    {
        public int id { get; set; }
        public string subject { get; set; }
        public string author { get; set; }
        public string body { get; set; }
    }
}
