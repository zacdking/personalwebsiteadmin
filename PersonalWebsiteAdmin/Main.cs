﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PersonalWebsiteAdmin
{
    public partial class Main : Form
    {
        public Main()
        {
            InitializeComponent();

            if (string.IsNullOrEmpty(Properties.Settings.Default.personalWebsiteURL))
            {
                Settings setting = new Settings();
                setting.ShowDialog();
            }

            populateDataGridView();
        }

        #region properties

        private PersonalWebsiteAPI.Model model = PersonalWebsiteAPI.Model.Comment;

        #endregion

        #region private methods

        /// <summary>
        /// Clears the dataGrid
        /// </summary>
        private void clearDataGridView()
        {
            dataGrid.Rows.Clear();
            dataGrid.Columns.Clear();
            dataGrid.Refresh();
        }

        /// <summary>
        /// Checks what Model is selected in View tab of ToolStrip, uses API to get all records of that model, then populates the dataGrid with the results
        /// </summary>
        private void populateDataGridView()
        {
            try
            {
                // If Comments is checked in View tab of ToolStrip
                if (commentsToolStripMenuItem.Checked)
                {
                    List<Comment> comments = PersonalWebsiteAPI.GetComments(Properties.Settings.Default.personalWebsiteURL);
                    model = PersonalWebsiteAPI.Model.Comment;
                    populateDataGridViewComments(comments);
                }
                // If Expressions is checked in View tab of ToolStrip
                else if (expressionsToolStripMenuItem.Checked)
                {
                    List<Expression> expressions = PersonalWebsiteAPI.GetExpressions(Properties.Settings.Default.personalWebsiteURL);
                    model = PersonalWebsiteAPI.Model.Expression;
                    populateDataGridViewExpressions(expressions);
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error");
            }
        }

        /// <summary>
        /// Formats dataGrid for the Comment model
        /// </summary>
        /// <param name="comments">Data Grid is populated with this list of comments</param>
        private void populateDataGridViewComments(List<Comment> comments)
        {
            clearDataGridView();

            dataGrid.ColumnCount = 4;

            dataGrid.Columns[0].Name = "ID";
            dataGrid.Columns[1].Name = "Subject";
            dataGrid.Columns[2].Name = "Author";
            dataGrid.Columns[3].Name = "Body";

            dataGrid.Columns[0].Width = 25;

            foreach (var comment in comments)
                dataGrid.Rows.Add(comment.id, comment.subject, comment.author, comment.body);
        }

        /// <summary>
        /// Formats dataGrid for the Expression model
        /// </summary>
        /// <param name="expressions">Data Grid is populated with this list of expressions</param>
        private void populateDataGridViewExpressions(List<Expression> expressions)
        {
            clearDataGridView();

            dataGrid.ColumnCount = 4;

            dataGrid.Columns[0].Name = "ID";
            dataGrid.Columns[1].Name = "Full";
            dataGrid.Columns[2].Name = "Simplified";
            dataGrid.Columns[3].Name = "Answer";

            dataGrid.Columns[0].Width = 25;

            foreach (var expression in expressions)
                dataGrid.Rows.Add(expression.id, expression.full, expression.simplified, expression.answer);
        }

        /// <summary>
        /// Displays the help menu
        /// </summary>
        private void displayHelpMenu()
        {
            MessageBox.Show("Help Menu", "Help");
        }

        /// <summary>
        /// Extracts data from selected row, inserts data back into a model, and carries data over to the edit menu
        /// </summary>
        /// <param name="rowIndex"></param>
        private void displayEditMenu(int rowIndex)
        {
            // Do nothing if Header was double clicked
            if (rowIndex == -1)
                return;

            if (model == PersonalWebsiteAPI.Model.Comment)
            {
                Comment comment = new Comment();
                comment.id = Int32.Parse(dataGrid.Rows[rowIndex].Cells[0].Value.ToString());
                comment.subject = dataGrid.Rows[rowIndex].Cells[1].Value.ToString();
                comment.author = dataGrid.Rows[rowIndex].Cells[2].Value.ToString();
                comment.body = dataGrid.Rows[rowIndex].Cells[3].Value.ToString();

                Edit edit = new Edit(Properties.Settings.Default.personalWebsiteURL, comment);
                edit.ShowDialog();
            }
            else if (model == PersonalWebsiteAPI.Model.Expression)
            {
                Expression expression = new Expression();
                expression.id = Int32.Parse(dataGrid.Rows[rowIndex].Cells[0].Value.ToString());
                expression.full = dataGrid.Rows[rowIndex].Cells[1].Value.ToString();
                expression.simplified = dataGrid.Rows[rowIndex].Cells[2].Value.ToString();
                expression.answer = dataGrid.Rows[rowIndex].Cells[3].Value.ToString();

                Edit edit = new Edit(Properties.Settings.Default.personalWebsiteURL, expression);
                edit.ShowDialog();
            }

            populateDataGridView();
        }

        #endregion

        #region events

        private void connectButton_Click(object sender, EventArgs e)
        {
            populateDataGridView();
        }

        private void commentsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            commentsToolStripMenuItem.Checked = true;
            expressionsToolStripMenuItem.Checked = false;
        }

        private void expressionsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            commentsToolStripMenuItem.Checked = false;
            expressionsToolStripMenuItem.Checked = true;
        }

        private void settingsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Settings settings = new Settings();
            settings.ShowDialog();
            populateDataGridView();
        }

        private void helpToolStripMenuItem_Click(object sender, EventArgs e)
        {
            displayHelpMenu();
        }

        private void dataGrid_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            displayEditMenu(e.RowIndex);
        }

        private void editButton_Click(object sender, EventArgs e)
        {
            if (dataGrid.SelectedRows.Count > 1)
                MessageBox.Show("Can't edit multiple records at the same time.", "Error");
            else
                displayEditMenu(dataGrid.SelectedRows[0].Index);
        }

        private void deleteButton_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow row in dataGrid.SelectedRows)
            {
                int id = Int32.Parse(dataGrid.Rows[row.Index].Cells[0].Value.ToString());
                PersonalWebsiteAPI.DeleteRecord(Properties.Settings.Default.personalWebsiteURL, model, id);
            }
            populateDataGridView();
        }

        private void clearButton_Click(object sender, EventArgs e)
        {
            PersonalWebsiteAPI.DeleteAll(Properties.Settings.Default.personalWebsiteURL, model);
            populateDataGridView();
        }

        private void wipeButton_Click(object sender, EventArgs e)
        {
            // For each model that exists 
            foreach (PersonalWebsiteAPI.Model m in (PersonalWebsiteAPI.Model[])Enum.GetValues(typeof(PersonalWebsiteAPI.Model)))
                PersonalWebsiteAPI.DeleteAll(Properties.Settings.Default.personalWebsiteURL, m);

            populateDataGridView();
        }

        #endregion

    }
}
