﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PersonalWebsiteAdmin
{
    public partial class Edit : Form
    {

        #region properties

        private List<TextBox> textBoxes = new List<TextBox>();
        private string personalWebsite;
        private PersonalWebsiteAPI.Model model;
        private int propertyInfoCount;

        #endregion

        #region constructors

        public Edit(string personalWebsiteURL, Comment comment)
        {
            InitializeComponent();
            personalWebsite = personalWebsiteURL;
            model = PersonalWebsiteAPI.Model.Comment;
            populate(comment);
        }

        public Edit(string personalWebsiteURL, Expression expression)
        {
            InitializeComponent();
            personalWebsite = personalWebsiteURL;
            model = PersonalWebsiteAPI.Model.Expression;
            populate(expression);
        }

        #endregion

        #region private methods

        /// <summary>
        /// Create labels and textboxes for every property inside the given model, display the given models' data, 
        /// store textboxes so that alter info can be extracted later.
        /// </summary>
        /// <typeparam name="T">Can be any model</typeparam>
        /// <param name="record">Selected record from main form</param>
        private void populate<T>(T record)
        {
            var propertyInfo = record.GetType().GetProperties();
            propertyInfoCount = propertyInfo.Length;

            // Allocate space for each property
            Size = new Size(500, Size.Height + propertyInfo.Length * 26);

            // Create Label and TextBox for each property
            for (int i = 0; i < propertyInfo.Length; i++)
            {
                Label label = new Label();
                TextBox textBox = new TextBox();

                Controls.Add(label);
                Controls.Add(textBox);

                label.Text = propertyInfo[i].Name;
                label.MaximumSize = new Size(85, 13);
                textBox.Text = propertyInfo[i].GetValue(record).ToString();

                label.Location = new Point(9, 15 + i * 26);
                textBox.Location = new Point(105, 12 + i * 25);
                textBox.Size = new Size(367, 20);

                // Expressions are immutable 
                if (model == PersonalWebsiteAPI.Model.Expression)
                    textBox.Enabled = false;
                
                // Insert new Text box inside the list so it may be referenced later
                textBoxes.Add(textBox);
            }
            // Assume first textBox is always the ID, something that shouldn't change
            textBoxes[0].Enabled = false;

            if (model == PersonalWebsiteAPI.Model.Expression)
                saveButton.Enabled = false;
        }

        #endregion

        #region events

        private void cancelButton_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void deleteButton_Click(object sender, EventArgs e)
        {
            int id = Int32.Parse(textBoxes[0].Text);

            // Send request to delete record
            try
            {
                PersonalWebsiteAPI.DeleteRecord(personalWebsite, model, id);
                MessageBox.Show("Record was deleted successfully.", "Success");
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error");
            }

            Close();
        }

        private void saveButton_Click(object sender, EventArgs e)
        {
            // Send request to edit record
            if (model == PersonalWebsiteAPI.Model.Comment)
            {
                Comment comment = new Comment
                {
                    id = Int32.Parse(textBoxes[0].Text),
                    subject = textBoxes[1].Text,
                    author = textBoxes[2].Text,
                    body = textBoxes[3].Text
                };

                // Send request to update record
                try
                {
                    PersonalWebsiteAPI.UpdateComment(personalWebsite, comment);
                    MessageBox.Show("Record was updated successfully.", "Success");
                }
                catch (Exception exception)
                {
                    MessageBox.Show(exception.Message, "Error");
                }
            }

            Close();
        }

        #endregion
    }
}
